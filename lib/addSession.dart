import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:developer';

import 'package:flutter_application_fire/Session_list.dart';

class AddSession extends StatefulWidget {
  const AddSession({Key? key}) : super(key: key);
  _AddSessionState createState() => _AddSessionState();
}

class _AddSessionState extends State<AddSession> {
  @override
  Widget build(BuildContext context) {
    TextEditingController exerciseController = TextEditingController();
    TextEditingController locationController = TextEditingController();
    TextEditingController hourController = TextEditingController();

    Future _addSession() {
      final exercise = exerciseController.text;
      final location = locationController.text;
      final hour = hourController.text;
      final ref = FirebaseFirestore.instance.collection("Session").doc();
      return ref
          .set({
            "Exercise_Name": exercise,
            "Location": location,
            "Hours": hour,
            "Doc_id": ref.id
          })
          .then((value) => log("Collection added!!"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                  controller: exerciseController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular((20)),
                      ),
                      hintText: "Enter Exercise name")),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: TextField(
                  controller: locationController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular((20)),
                      ),
                      hintText: "Enter Location")),
            ),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                    controller: hourController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular((20)),
                        ),
                        hintText: "Enter Number of hours in words"))),
            ElevatedButton(
                onPressed: () {
                  _addSession();
                },
                child: Text("Add Session"))
          ],
        ),
        const Session_list()
      ],
    );
  }
}
