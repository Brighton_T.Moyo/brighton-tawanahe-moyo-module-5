import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'dart:html';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Session_list extends StatefulWidget {
  const Session_list({Key? key}) : super(key: key);

  @override
  State<Session_list> createState() => _Session_listState();
}

class _Session_listState extends State<Session_list> {
  final Stream<QuerySnapshot> _mySession =
      FirebaseFirestore.instance.collection("Session").snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _mySession,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                      height: (MediaQuery.of(context).size.height),
                      width: (MediaQuery.of(context).size.width),
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data =
                              documentSnapshot.data()! as Map<String, dynamic>;
                          return ListTile(
                            title: Text(data['Exercise_Name']),
                            subtitle: Text(data['Hours']),
                            isThreeLine: true,
                            dense: true,
                          );
                        }).toList(),
                      )))
            ],
          );
        } else {
          return (Text("No data"));
        }
      },
    );
  }
}
